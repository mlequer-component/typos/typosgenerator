<?php

namespace MLequer\Component\Typos\Generator;

use MLequer\Component\Typos\Provider\TyposProviderInterface;

abstract class AbstractTyposGenerator implements TyposGeneratorInterface
{
    /**
     * @var TyposProviderInterface
     */

    public function __construct(protected TyposProviderInterface $typoProvider)
    {
    }
}
